﻿using InTheHand.Bluetooth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bttest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            AMain().Wait();
        }

        private static async Task AMain()
        {
            string targetDeviceName = "Ember Ceramic Mug";
            BluetoothDevice emberMugBTDevice = null;
            while (emberMugBTDevice == null)
            {
                Console.WriteLine("Starting device scan");
                BluetoothDevice[] devices = (await Bluetooth.ScanForDevicesAsync()).ToArray();
                emberMugBTDevice = devices.FirstOrDefault(el => el.Name == targetDeviceName);
            }
            Console.WriteLine($"Device located ({emberMugBTDevice.Id})");
            
            if(!emberMugBTDevice.IsPaired)
            {
                Console.WriteLine("Device is not paired");
                //emberMugBTDevice.PairAsync();
            } else
            {
                Console.WriteLine("Device is paired");
            }

            Console.WriteLine("End");
            Console.ReadLine();
        }
    }
}
