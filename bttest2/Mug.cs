﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;

namespace bttest2
{

    internal enum Characteristic
    {
        LIQUID_TEMPERATURE,
        BATTERY_STATS,
        LIQUID_PRESENCE,
        TARGET_TEMPERATURE,
        LIQUID_STATE,
    }

    internal enum State
    {
        Heating,
        Cooling,
        Stable
    }

    internal class Mug
    {
        internal static Dictionary<Characteristic, Guid> CHARACTERISTICS = new Dictionary<Characteristic, Guid>()
        {
            { Characteristic.LIQUID_TEMPERATURE, Guid.Parse("fc540002-236c-4c94-8fa9-944a3e5353fa") },
            { Characteristic.BATTERY_STATS, Guid.Parse("fc540007-236c-4c94-8fa9-944a3e5353fa") },
            { Characteristic.LIQUID_PRESENCE, Guid.Parse("fc540005-236c-4c94-8fa9-944a3e5353fa") },
            { Characteristic.TARGET_TEMPERATURE, Guid.Parse("fc540003-236c-4c94-8fa9-944a3e5353fa") },
            { Characteristic.LIQUID_STATE, Guid.Parse("fc540008-236c-4c94-8fa9-944a3e5353fa") },
        };
        internal static ulong MUG_ADDRESS = 0xF5A2E215E5C0;
        internal static Guid CONTROL_SERVICE_GUID = Guid.Parse("fc543622-236c-4c94-8fa9-944a3e5353fa");

        private BluetoothLEDevice? device = null;
        private GattDeviceService? service = null;
        private float stateLiquidTemperature;
        private float stateBatteryCharge;
        private bool stateBatteryIsCharging;
        private float stateBatteryTemperature;
        private bool stateLiquidPresent;
        private float stateTargetTemperature;
        private State stateLiquidState;

        internal float LiquidTemperature => stateLiquidTemperature;
        internal float BatteryCharge => stateBatteryCharge;
        internal bool Charging => stateBatteryIsCharging;
        internal float BatteryTemperature => stateBatteryTemperature;
        internal bool LiquidPresent => stateLiquidPresent;
        internal float TargetTemperature => stateTargetTemperature;
        internal State LiquidState => stateLiquidState;

        private async Task Reconnect()
        {
            void pairingHandler(DeviceInformationCustomPairing customPairing, DevicePairingRequestedEventArgs args)
            {
                args.Accept("0");
            }

            BluetoothLEDevice device = await BluetoothLEDevice.FromBluetoothAddressAsync(MUG_ADDRESS);
            device.DeviceInformation.Pairing.Custom.PairingRequested += pairingHandler;
            if (!device.DeviceInformation.Pairing.IsPaired)
            {
                DevicePairingResult pairingResult = await device.DeviceInformation.Pairing.Custom.PairAsync(DevicePairingKinds.ConfirmOnly, DevicePairingProtectionLevel.None);
                if (pairingResult.Status != DevicePairingResultStatus.Paired)
                {
                    await Console.Out.WriteLineAsync("Pairing failure");
                    this.device = null;
                    this.service = null;
                    return;
                }
            }

            var resp = await device.GetGattServicesForUuidAsync(CONTROL_SERVICE_GUID, BluetoothCacheMode.Uncached);
            if (resp.Status != GattCommunicationStatus.Success) { 
                this.device = null;
                this.service = null;
                return;

            }
            GattDeviceService service = resp.Services[0]; // TODO validate successful fetch
            if((await service.OpenAsync(GattSharingMode.SharedReadAndWrite)) != GattOpenStatus.Success) {
                this.device = null;
                this.service = null;
                return;
            }

            this.device = device;
            this.service = service;
        }

        internal async Task RefreshData()
        {
            if (device == null)
                await Reconnect();

            stateLiquidTemperature = ParseTemperature(await ReadValueFromDevice(Characteristic.LIQUID_TEMPERATURE));
            var batteryStats = ParseBatteryStats(await ReadValueFromDevice(Characteristic.BATTERY_STATS));
            stateBatteryCharge = batteryStats.Item1;
            stateBatteryIsCharging = batteryStats.Item2;
            stateBatteryTemperature = batteryStats.Item3;
            stateLiquidPresent = ParseLiquidPresence(await ReadValueFromDevice(Characteristic.LIQUID_PRESENCE));
            stateTargetTemperature = ParseTemperature(await ReadValueFromDevice(Characteristic.TARGET_TEMPERATURE));
            stateLiquidState = ParseLiquidState(await ReadValueFromDevice(Characteristic.LIQUID_STATE));
        }

        private static State ParseLiquidState(byte[]? bytes)
        {
            if (bytes == null)
            {
                return State.Stable;
            }

            switch (bytes[0])
            {
                case 0x4:
                    return State.Cooling;
                case 0x5:
                    return State.Heating;
                default:
                    return State.Stable;
            }
        }

        private static bool ParseLiquidPresence(byte[]? bytes)
        {
            if (bytes == null)
                return false;

            return bytes[0] == 30;
        }

        private static Tuple<float, bool, float> ParseBatteryStats(byte[]? bytes)
        {
            if (bytes == null)
                return new Tuple<float, bool, float>(0, false, 0);

            float batteryCharge = bytes[0] * 0.01f;
            bool batteryIsCharging = bytes[1] == 0b1;
            float batteryTemperature = ParseTemperature(new byte[] { bytes[2], bytes[3] });
            return new Tuple<float, bool, float>(batteryCharge, batteryIsCharging, batteryTemperature);
        }

        internal async Task SetTargetTemperature(float temperature)
        {
            await WriteValueToDevice(Characteristic.TARGET_TEMPERATURE, EncodeTemperature(temperature));
            await RefreshData();
        }

        private static float ParseTemperature(byte[]? bytes)
        {
            if (bytes == null)
                return 0;

            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
            ushort i = BitConverter.ToUInt16(bytes, 0);
            return i * 0.01f;
        }

        private static byte[] EncodeTemperature(float temperature)
        {
            ushort clampedTemp = (ushort)(temperature * 100);
            byte[] res = BitConverter.GetBytes(clampedTemp);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(res);
            }
            return res;
        }

        private async Task<byte[]?> ReadValueFromDevice(Characteristic characteristic)
        {
            if (service == null)
                return null;

            var characteristics = service.GetCharacteristics(CHARACTERISTICS[characteristic]).First(); // TODO handle missing characteristic possibility
            if (characteristics == null)
                throw new Exception("Unable to get characteristic");
            var readResult = await characteristics.ReadValueAsync(BluetoothCacheMode.Uncached);
            if (readResult.Status != GattCommunicationStatus.Success)
                throw new Exception("Unable to read value");
            return readResult.Value.ToArray();
        }

        private async Task WriteValueToDevice(Characteristic characteristic, byte[] value)
        {
            if (service == null)
                return;

            var characteristics = service.GetCharacteristics(CHARACTERISTICS[characteristic]).First(); // TODO handle missing characteristic possibility
            if (characteristics == null)
                throw new Exception("Unable to get characteristic");
            var writeResult = await characteristics.WriteValueWithResultAsync(value.AsBuffer());
            if (writeResult.Status != GattCommunicationStatus.Success)
                throw new Exception("Unable to write value");
        }

    }
}
