﻿//using InTheHand.Bluetooth;
using bttest2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
//using Windows.Devices.Bluetooth;

namespace bttest
{
    internal class Program
    {
        private static HttpListener? listener;

        static void Main(string[] args)
        {
            listener = new HttpListener();
            var url = "http://127.0.0.1:14274/";
            listener.Prefixes.Add(url);
            listener.Start();
            Console.WriteLine($"Listening for connections on {url}");

            Task listenTask = HandleConnections();
            listenTask.GetAwaiter().GetResult();

            listener.Close();
        }

        private static async Task HandleConnections()
        {
            if (listener == null)
                throw new Exception("Server is not listening");

            var mug = new Mug();
            while(listener.IsListening)
            {
                HttpListenerContext ctx = await listener.GetContextAsync();

                HttpListenerRequest request = ctx.Request;
                HttpListenerResponse response = ctx.Response;

                if (request.Url == null)
                {
                    response.Close();
                    continue;
                }

                response.ContentType = "text/plain";

                if (request.HttpMethod == "GET")
                {
                    try
                    {
                        await mug.RefreshData();
                    } catch
                    {
                        await Task.Delay(10000);
                    }
                    string responseStr = "";
                    switch (request.Url.AbsolutePath)
                    {
                        case "/liquid-temperature":
                            responseStr = $"{mug.LiquidTemperature}";
                            break;
                        case "/battery-charge":
                            responseStr = $"{mug.BatteryCharge}";
                            break;
                        case "/charging":
                            responseStr = mug.Charging ? "1" : "0";
                            break;
                        case "/battery-temperature":
                            responseStr = $"{mug.BatteryTemperature}";
                            break;
                        case "/liquid-present":
                            responseStr = mug.LiquidPresent ? "1" : "0";
                            break;
                        case "/target-temperature":
                            responseStr = $"{mug.TargetTemperature}";
                            break;
                        case "/thermal-state":
                            responseStr = $"{mug.LiquidState}";
                            break;
                        default:
                            responseStr = "ERR";
                            break;
                    }
                    response.StatusCode = 200;
                    byte[] buffer = Encoding.UTF8.GetBytes(responseStr);
                    await response.OutputStream.WriteAsync(buffer, 0, buffer.Length);

                } else if (request.HttpMethod == "POST")
                {
                    if (request.Url.AbsolutePath == "/target-temperature")
                    {
                        StreamReader reader = new StreamReader(request.InputStream);
                        string text = reader.ReadToEnd();
                        float targetTemperature = float.Parse(text);
                        try
                        {
                            await mug.SetTargetTemperature(targetTemperature);
                        } catch
                        {
                            await Task.Delay(10000);
                        }
                        response.StatusCode = 200;
                    }
                }

                response.Close();
            }
        }


    }
}
